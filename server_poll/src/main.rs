use std::time::Duration;
use std::net::TcpStream;
use std::net::TcpListener;
use std::io::Write;
use std::thread;
use rand::Rng;
use std::io;

fn main() -> std::io::Result<()> {

    println!("3 servers have started");
    let server1 = TcpListener::bind("127.0.0.1:2020").unwrap();
    let server2 = TcpListener::bind("127.0.0.1:2021").unwrap();
    let server3 = TcpListener::bind("127.0.0.1:2022").unwrap();

    server1.set_nonblocking(true);
    server2.set_nonblocking(true);
    server3.set_nonblocking(true);

    thread::spawn( move || {
        for stream in server1.incoming() {
            println!{"Server 1 - Stream"};
            match stream {
                Ok(str) => {
                      thread::spawn(|| {
                        handle_client(1, str);
                      });
                      println!{"Server 1 -> Finished"};
                      thread::sleep(Duration::from_secs(5));
                }
                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                    println!("Server 1 - WouldBlock");
                    continue;
                }
                Err(e) => panic!("encountered IO error: {e}"),
            }
        }
    });


  thread::spawn( move || {
        for stream in server2.incoming() {
            println!{"Server 2 - Stream"};
            match stream {
                Ok(str) => {
                      thread::spawn(|| {
                        handle_client(2, str);
                    });
                    println!{"Server 2 -> Finished"};
                    thread::sleep(Duration::from_secs(5));
                }
                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                    println!("Server 2 - WouldBlock");
                    continue;
                }
                Err(e) => panic!("encountered IO error: {e}"),
            }   
    }});


    thread::spawn( move || {
        for stream in server3.incoming() {
            println!{"Server 3 - Stream"};
            match stream {
                Ok(str) => {
                      thread::spawn(|| {
                        handle_client(3, str);
                      });
                      println!{"Server 3 -> Finished"};
                      thread::sleep(Duration::from_secs(5));
                }
                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                    println!("Server 3 - WouldBlock");
                    continue;
                }
                Err(e) => panic!("encountered IO error: {e}"),
            }
        }
    }); 

loop {}

} 

fn handle_client(label: u32, mut stream: TcpStream) {
    println!("Handle Client {:?}, {:?}", label, stream);
     if label == 1 {
        loop {
            let ran = rand::thread_rng().gen_range(8..10);
            thread::sleep(Duration::from_secs(ran));
            let s = format!("Message: Server 1 - inactive for {:?}, {}", ran, "sec");
            stream.write(&s.as_bytes()).unwrap();
        }         
     } else if label == 2 {
        loop {
             let ran = rand::thread_rng().gen_range(5..8);
             thread::sleep(Duration::from_secs(ran));
             let s = format!("Message: Server 2 - inactive for {:?}, {}", ran, "sec");
             stream.write(&s.as_bytes()).unwrap();
         } 
     } else if label == 3 {
        loop {
             let ran = rand::thread_rng().gen_range(1..4);
             thread::sleep(Duration::from_secs(ran));
             let s = format!("Message: Server 3 - inactive for {:?}, {}", ran, "sec");
             stream.write(&s.as_bytes()).unwrap();
         }
     }
}