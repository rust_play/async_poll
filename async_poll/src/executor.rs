
use std::net::TcpStream;
use std::sync::mpsc::{ Receiver};
use super::reactor::Reactor;

pub struct Executor<'a> {
    client: &'a Vec<TcpStream>,
    events: Vec<(usize, Box<dyn FnMut()>)>,
    ev_receiver: Receiver<usize>,
}

impl<'a> Executor<'a> {

    pub fn new(ev_receiver: Receiver<usize>, reactor:  &'a Reactor) -> Executor<'a> {
        Executor{ client: reactor.get_client(), events: vec![], ev_receiver }
    }

    pub fn register_event(&mut self, id: usize, fun_exec: impl FnMut() + 'static) {
        self.events.push((id, Box::new(fun_exec)));
    }

    fn execute(&mut self, event: usize) {
        let (_, registered_function) = self.events.iter_mut() .find(|(e, _)| *e == event).expect("Event not found");
        registered_function();
    }

    pub fn receive_token(&mut self) {
        while let Ok(token) = self.ev_receiver.recv() {
            if token == 1 {
                self.execute(token);
            }
            if token == 2 {
                self.execute(token);
            }
            if token == 3 {
                self.execute(token);
            }
        }
    }

}