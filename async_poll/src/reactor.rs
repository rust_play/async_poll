use polling::{Event, Poller};
use std::io::{Read, self};
use std::net::{TcpStream};
use std::{thread};
use std::sync::mpsc::{Sender};
use std::sync::Arc;
use std::str;

pub struct Reactor {
    client: Arc<Vec<TcpStream>>,
    poller: Arc<Poller>
}

impl Reactor {

    pub fn new( sockets: Vec<TcpStream> ) -> Reactor {
        let poller: Arc<Poller> = Arc::new(Poller::new().unwrap());
        let client: Arc<Vec<TcpStream>> = Arc::new(sockets);
        Reactor { client, poller }
    }

    pub fn register_interest(&self){
       self.poller.add(&self.client[0], Event::readable(1)).unwrap();
       self.poller.add(&self.client[1], Event::readable(2)).unwrap();
       self.poller.add(&self.client[2], Event::readable(3)).unwrap();
    }

    pub fn poll_events(&self, evt_sender: Sender<usize>) {

        let mut events = Vec::new();
        let poller = self.poller.clone();
        let client = self.client.clone();
        thread::spawn( move || {

            loop {
                // Waiting for I/O event.
                events.clear();
                  match poller.wait(&mut events, None) {
                    Ok(e) => 
                    for ev in &events {
                        if ev.key == 1 && ev.readable && !ev.writable {
                           poller.modify(&client[0], Event::readable(ev.key)).unwrap();
                           evt_sender.send(ev.key).expect("send event_token err.");
                       } 
                       if ev.key == 2 && ev.readable && !ev.writable {
                           poller.modify(&client[1], Event::readable(ev.key)).unwrap();
                           evt_sender.send(ev.key).expect("send event_token err.");
                       } 
                       if ev.key == 3 && ev.readable && !ev.writable {
                           poller.modify(&client[2], Event::readable(ev.key)).unwrap();
                           evt_sender.send(ev.key).expect("send event_token err.");
                       } 
                    }
                    
                    Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                        println!("WouldBlock: {}", e);
                        break;
                    }
                    Err(e) => panic!("Poll error: {:?}, {}", e.kind(), e),
                }
            }
        });
    }

    pub fn get_client(&self) -> &Vec<TcpStream> {
        let vector = &*self.client;
        return vector
    }

}