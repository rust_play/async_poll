use std::io::{Write, Read, self};
use std::sync::mpsc::channel;
use std::net::TcpStream;
use std::rc::Rc;
mod executor;
mod reactor;
use std::{str};


fn main() {

    //Mpsc Channel
    let (ev_sender, ev_receiver) = channel();

    let client1 = TcpStream::connect("127.0.0.1:2020").unwrap();
    let client2 = TcpStream::connect("127.0.0.1:2021").unwrap();
    let client3 = TcpStream::connect("127.0.0.1:2022").unwrap();

    client1.set_nonblocking(true);
    client2.set_nonblocking(true);
    client3.set_nonblocking(true);
       
    //Sockets
    let sockets: Vec<TcpStream> = vec!{client1, client2 , client3};

    //Reactor
    let reactor = reactor::Reactor::new(sockets);
    reactor.register_interest();
    reactor.poll_events(ev_sender);
   
    let reactor_rc = Rc::new(reactor);
    let reactor_clone_1 = Rc::clone(&reactor_rc);
    let reactor_clone_2 = Rc::clone(&reactor_rc);
    let reactor_clone_3 = Rc::clone(&reactor_rc);

    //Executor
    let mut executor = executor::Executor::new(ev_receiver, &reactor_rc);

     executor.register_event(1, move || {
        let client_list: &Vec<TcpStream> = &reactor_clone_1.get_client();
        let mut client_zero = &client_list[0];
         let mut buffer = [0; 40];
         match  client_zero.read(&mut buffer) {
            Ok(bytes_read) => {
                println!("Read {:?}", str::from_utf8(&buffer[..bytes_read]));
            }
            Err(e) if e.kind() == std::io::ErrorKind::ConnectionAborted => {
                println!("Client 0 - Disconnected");
            }
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                println!("Client 0 - WouldBlock");
            }
            Err(e) => {
                println!("Client 0 - Other error: {e}");
            }
        }
    }); 

    executor.register_event(2, move || {
        let client_list: &Vec<TcpStream> = &reactor_clone_2.get_client();
        let mut client_one = &client_list[1];
         let mut buffer = [0; 40];
         match  client_one.read(&mut buffer) {
            Ok(bytes_read) => {
                println!("Read {:?}", str::from_utf8(&buffer[..bytes_read]));
            }
            Err(e) if e.kind() == std::io::ErrorKind::ConnectionAborted => {
                println!("Client 1 - Disconnected");
            }
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                println!("Client 1 - WouldBlock");
            }
            Err(e) => {
                println!("Client 1 - Other error: {e}");
            }
        }
    }); 

    executor.register_event(3, move || {
        let client_list: &Vec<TcpStream> = &reactor_clone_3.get_client();
        let mut client_two = &client_list[2];
         let mut buffer = [0; 40];
         match  client_two.read(&mut buffer) {
            Ok(bytes_read) => {
                println!("Read {:?}", str::from_utf8(&buffer[..bytes_read]));
            }
            Err(e) if e.kind() == std::io::ErrorKind::ConnectionAborted => {
                println!("Client 2 - Disconnected");
            }
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                println!("Client 2 - WouldBlock");
            }
            Err(e) => {
                println!("Client 2 - Other error: {e}");
            }
        }
    }); 

    executor.receive_token();
}
